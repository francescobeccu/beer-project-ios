//
//  Utility.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import UIKit
import Foundation

class Utility {
    
    // MARK: Show Alert
    static func showInfoAlert(message : String, fromView: UIViewController) {
        let alert = UIAlertController(title: "O2O - Beer Project", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        fromView.present(alert, animated: true, completion: nil)
       
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    var intValue: Int {
        return (self as NSString).integerValue
    }
    
    var doubleValue: Double {
        return (self as NSString).doubleValue
    }
}

