//
//  Navigation.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import Foundation
import UIKit
import AVFoundation

class Navigation: NSObject {
    
    static let shared = Navigation()
    
    
    func showHome(_ storyboard: UIStoryboard, _ beers : [Beer]) {
        let home = storyboard.instantiateViewController(withIdentifier: "homeNavigationID") as! UINavigationController
        if let homeViewController = home.topViewController {
            if homeViewController.isKind(of: HomeViewController.classForCoder()) {
                (homeViewController as! HomeViewController).beers = beers
            }
        }
        setCurrentRootViewController(home)
    }
    
    // MARK: Root View Controller
    func currentRootViewController() -> UIViewController {
        return UIApplication.shared.windows.first!.rootViewController!
    }
    
    func setCurrentRootViewController(_ viewController: UIViewController) {
        UIApplication.shared.windows.first!.rootViewController = viewController
    }
    
    // MARK: Navigation Controller
    func currentNavigationController() -> UINavigationController? {
        if currentRootViewController().navigationController != nil {
            return currentRootViewController().navigationController
        }
        return nil
    }
}
