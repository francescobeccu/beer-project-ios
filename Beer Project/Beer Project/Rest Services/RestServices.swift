//
//  RestServices.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import Foundation
import UIKit
import MobileCoreServices
import CoreLocation

class RestServices: NSObject {
    
    // MARK: Service Configuration
    static func getBeers(_ completion: @escaping (_ result : String, _ error : String) -> Void) {
        BeerService().serviceGetBeers{ (result, error) in
            completion(result, error)
        }
    }
    
    static func getBeersPaginated(_ page : Int, _ completion: @escaping (_ result : String, _ error : String, _ objectIds : [NSNumber]) -> Void) {
        self.getBeersPaginated(page, "", "") { result, error, objectIds in
            completion(result, error, objectIds)
        }
    }
    
    static func getBeersPaginated(_ page : Int, _ keyword : String, _ keywordValue : String, _ completion: @escaping (_ result : String, _ error : String, _ objectIds : [NSNumber]) -> Void) {
        
        BeerService().serviceGetBeersPaginated(page, keyword, keywordValue) { result, error, objectIds in
            completion(result, error, objectIds)
        }
    }
}
