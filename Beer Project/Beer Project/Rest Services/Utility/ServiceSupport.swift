//
//  ServiceSupport.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import Alamofire
import Foundation

// MARK:  Response status
var FAIL : String = "FAIL"
var SUCCESS : String = "SUCCESS"

// MARK:  Response errors
let GENERIC_ERROR : String = "GENERIC_ERROR"
let SYSTEM_ERROR : String = "SYSTEM_ERROR"
let BAD_REQUEST : String = "BAD_REQUEST"
let UNAUTHORIZED_ACCESS : String = "UNAUTHORIZED_ACCESS"
let INVALID_USER_TOKEN : String = "INVALID_USER_TOKEN"
let INVALID_UUID : String = "INVALID_UUID"
let INVALID_CREDENTIALS : String = "INVALID_CREDENTIALS"
let USER_NOT_VERIFIED : String = "USER_NOT_VERIFIED"
let USER_EXISTS : String = "USER_EXISTS"
let USER_CANT_ACCESS : String = "USER_CANT_ACCESS"

let INVALID_EMAIL_PASSWORD : String = "INVALID_EMAIL_PASSWORD"

class ServiceSupport: NSObject {
    
    static let sharedInstance = ServiceSupport()
    
    // MARK: Header Alamofire
    func headerAlamofire() -> HTTPHeaders {
        
        return HTTPHeaders.init()
//        HTTPHeaders.init(["lang" : "it",
//                          "devicePlatform" : "IOS",
//                          "deviceType" : "MOBILE"])
    }
    
    func headerIsOk(dictResponse : NSDictionary) -> Bool {
        if dictResponse.object(forKey: "result") == nil {
            return false
        }
        let result : NSString = dictResponse.object(forKey: "result") as! String as NSString
        if (result as String == FAIL) {
            return false
        } else {
            return true
        }
    }
}
    
