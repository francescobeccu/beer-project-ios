//
//  ServiceConstants.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import Foundation

// MARK:  Base Url
let BASE_URL : String = "https://api.punkapi.com/"


let API_VERSION : String = "v2"
let BASE_PATH : String = BASE_URL + API_VERSION

let RESULT_PER_PAGE : Int = 7
