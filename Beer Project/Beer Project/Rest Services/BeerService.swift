//
//  BeerService.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import Foundation
import UIKit
import Alamofire

// MARK:  Api Paths
let SERVICE_BEERS_PATH : String = "/beers"


class BeerService: NSObject {
    
    var request: Alamofire.Request? {
        didSet {
            oldValue?.cancel()
        }
    }
    
    // MARK: Service Beers
    func serviceGetBeers(_ completion: @escaping (_ result : String, _ error : String) -> Void) {
        
        print("<--- Service Get Beers --->")
        
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        
        let completePath = "\(BASE_PATH)\(SERVICE_BEERS_PATH)"
        
        AF.request(completePath, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: ServiceSupport.sharedInstance.headerAlamofire()).responseJSON
            { response in switch response.result {
                        case .success(let JSON):
                            // print("Success with JSON: \(JSON)")
                            BPCoreDataUtility.shared.saveBeers(JSON as! NSArray)
                            completion(SUCCESS, "")
                            break

                        case .failure(let error):
                            completion(FAIL, "generic.error")
                            print("Request failed with error: \(error)")
                            break
                        }
            }
    }
    
    func serviceGetBeersPaginated(_ page : Int, _ completion: @escaping (_ result : String, _ error : String, _ objectIds : [NSNumber]) -> Void) {
        self.serviceGetBeersPaginated(page, "", "") { result, error, objectIds in
            completion(result, error, objectIds)
        }
    }
    
    func serviceGetBeersPaginated(_ page : Int, _ keyword : String, _ keywordValue : String, _ completion: @escaping (_ result : String, _ error : String, _ objectIds : [NSNumber]) -> Void) {
        
        print("<--- Service Get Beers --->")
        
        var completePath = String(format: "%@%@?page=\(page)&per_page=\(RESULT_PER_PAGE)", BASE_PATH, SERVICE_BEERS_PATH, keyword)
        if !keywordValue.isEmpty {
            completePath.append(String(format:"&%@=%@", keyword,keywordValue))
        }
        
        AF.request(completePath, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: ServiceSupport.sharedInstance.headerAlamofire()).responseJSON
            { response in switch response.result {
                        case .success(let JSON):
                            // print("Success with JSON: \(JSON)")
                            let ids = BPCoreDataUtility.shared.saveBeers(JSON as! NSArray)
                            completion(SUCCESS, "", ids)
                            break

                        case .failure(let error):
                            completion(FAIL, "generic.error", [])
                            print("Request failed with error: \(error)")
                            break
                        }
            }
    }
}
