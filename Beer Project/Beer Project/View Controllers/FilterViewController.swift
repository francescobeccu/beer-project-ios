//
//  FilterViewController.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import Foundation

import UIKit

class FilterViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let filters : [[String:String]] = [["label":"Alchol > 7.0 %", "value":"7.0"],["label":"Alchol > 4.5 %", "value":"4.5"]]

    var filterDelegate: FilterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.reloadData()
    }
    
    // MARK: Table View
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
       }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filters.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "filtersCell") {
            
            let filterDict = filters[indexPath.row] as NSDictionary
            cell.textLabel?.text = filterDict["label"] as? String
            
            return cell
        }
    
        return UITableViewCell()
            
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let filterDict = filters[indexPath.row] as NSDictionary
        if let delegate = self.filterDelegate {
            delegate.sendData(filter: filterDict["value"] as! String)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

