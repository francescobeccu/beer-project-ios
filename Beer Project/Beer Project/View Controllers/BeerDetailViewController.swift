//
//  BeerDetailViewController.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import Foundation
import UIKit

class BeerDetailViewController: BaseViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var beerImage : UIImageView!
    @IBOutlet weak var tagline : UILabel!
    @IBOutlet weak var beerDescription : UILabel!
    @IBOutlet weak var brewersTips : UILabel!
    
    @IBOutlet weak var alcoholLabel : UILabel!
    @IBOutlet weak var ibuLabel : UILabel!
    @IBOutlet weak var phLabel : UILabel!
    @IBOutlet weak var firstBrewedLabel : UILabel!
    
    @IBOutlet weak var alcoholValue : UILabel!
    @IBOutlet weak var ibuValue : UILabel!
    @IBOutlet weak var phValue : UILabel!
    @IBOutlet weak var firstBrewedValue : UILabel!
    
    @IBOutlet weak var foodPairingLabel : UILabel!
    @IBOutlet weak var tableView : UITableView!
    
    var currentBeer : Beer!
    
    var foodPairing : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = self.currentBeer.beer_name
        
        self.configureView()
    }
    
    func configureView() {
        
        self.tagline.text = currentBeer.tagline
        self.beerDescription.text = currentBeer.beer_description
        self.brewersTips.text = currentBeer.brewers_tips
        
        if let url: URL = URL(string: currentBeer.image_url!) {
            self.beerImage.sd_setImage(with: url, completed: nil)
        }
        
        self.alcoholLabel.text = "alcohol.label".localized
        self.ibuLabel.text = "ibu.label".localized
        self.phLabel.text = "ph.label".localized
        self.firstBrewedLabel.text = "firstBrewed.label".localized
        
        self.alcoholValue.text = String(format: "%.2f", currentBeer.abv)
        self.ibuValue.text = String(format: "%.2f", currentBeer.ibu)
        self.phValue.text = String(format: "%.2f", currentBeer.ph)
        self.firstBrewedValue.text = currentBeer.first_brewed
        
        self.foodPairing = currentBeer.food_pairing as! [String]
        if self.foodPairing.count > 0 {
            self.tableView.reloadData()
        } else {
            self.foodPairingLabel.isHidden = true
        }
    }
    
    // MARK: Table View
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 44
       }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return foodPairing.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "foodCell") {
            
            cell.textLabel?.text = foodPairing[indexPath.row]
            
            return cell
        }
    
        return UITableViewCell()
            
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

