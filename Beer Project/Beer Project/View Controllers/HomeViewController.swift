//
//  HomeViewController.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//


import Foundation
import UIKit

protocol FilterProtocol: AnyObject {
    func sendData(filter: String)
}

class HomeViewController: BaseViewController, FilterProtocol, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var beers : [Beer] = []
    var filteredBeers : [Beer] = []
    var noResult = false
    
    var searchWordKey = ""
    var searchWord = ""
    
    // pagination
    var isLoadingStarted = false
    var currentPage = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "home.title".localized
      
        
        refreshControl.addTarget(self, action:
                                    #selector(resetPaginationOrFilter), for: UIControl.Event.valueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.configureFilterButton()
    }
    
    // MARK: Table View
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
       }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if noResult {
            return 1
        }
           return beers.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if noResult {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "beerCellNoResult") as? BeerCellNoResult {
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "beerCell") as? BeerCell {
                
                let beer = self.beers[indexPath.row]
                
                cell.configureCell(beer)
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let beerDetail = self.storyboard!.instantiateViewController(withIdentifier: "beerDetailID") as! BeerDetailViewController
        
        let beer = self.beers[indexPath.row]
        beerDetail.currentBeer = beer
        
        self.navigationController!.pushViewController(beerDetail, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       let lastSectionIndex = tableView.numberOfSections - 1
       let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
       if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && isLoadingStarted {
          // print("this is the last cell")
        let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
           spinner.startAnimating()
           spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))

        self.tableView.tableFooterView = spinner
        self.tableView.tableFooterView?.isHidden = false
        DispatchQueue.main.async(execute: {
            
        })
          
       }
   }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height {
            
            if isLoadingStarted {
                isLoadingStarted = false
                self.searchWordKey = "food"
                self.loadMoreData {
                    self.tableView.tableFooterView?.isHidden = true
                    self.tableView.reloadData()
                }
                
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        self.searchWord = searchText
        self.searchWordKey = "food"
        self.filterBeersSearch(searchText)
    }
    func filterBeersSearch(_ searchText : String) {
        
        RestServices.getBeersPaginated(currentPage, self.searchWordKey, searchText) { (result, error, objectIds) in
          
            if result == SUCCESS {
                self.noResult = false
                self.beers = BPCoreDataUtility.shared.getBeerWithId(objectIds)
                if self.beers.count == 0 {
                    self.noResult = true
                    Utility.showInfoAlert(message: "no.result.founded".localized,fromView: self)
                    self.tableView.tableFooterView?.isHidden = true
                }
            } else {
                Utility.showInfoAlert(message: error,fromView: self)
            }
            
            self.tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.resetPaginationOrFilter()
    }
    
    
    // MARK: load more beers
    func loadMoreData(_ completion:@escaping (() -> Void)) {
        currentPage += 1
        
        RestServices.getBeersPaginated(currentPage, self.searchWordKey, self.searchWord) { (result, error, objectIds) in
           
            if result == SUCCESS {
                let newBeers = BPCoreDataUtility.shared.getBeerWithId(objectIds)
                for b in newBeers {
                    self.beers.append(b)
                }
                self.tableView.tableFooterView?.isHidden = true
                self.tableView.reloadData()
            } else {
                Utility.showInfoAlert(message: error,fromView: self)
            }
        }
    }
    
    @objc func resetPaginationOrFilter() {
        
        self.currentPage = 1
        self.searchWord = ""
        self.searchWordKey = ""
        
        self.configureFilterButton()
        self.filterBeersFilters()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.isLoadingStarted = true
    }
    
    func sendData(filter: String) {
        
        self.searchWord = filter
        self.searchWordKey = "abv_gt"
        self.currentPage = 1
        
        self.configureResetFiltersButton()
        
        self.filterBeersFilters()
    }

    // MARK: Navigation Button
    @IBAction func showFiltersTable() {
        
        let filters = self.storyboard!.instantiateViewController(withIdentifier: "showFiltersID") as! FilterViewController
        filters.filterDelegate = self
        self.present(filters, animated: true, completion: nil)
    }
    
    func filterBeersFilters() {
        
        RestServices.getBeersPaginated(currentPage, self.searchWordKey, self.searchWord) { (result, error, objectIds) in
          
            if result == SUCCESS {
                
                self.noResult = false
                self.beers = BPCoreDataUtility.shared.getBeerWithId(objectIds)
                if self.beers.count == 0 {
                    self.noResult = true
                    Utility.showInfoAlert(message: "no.result.founded".localized,fromView: self)
                }
            } else {
                Utility.showInfoAlert(message: error,fromView: self)
            }
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            self.tableView.reloadData()
        }
    }
    
    
    // MARK: Filter buttons
    func configureFilterButton() {
        let filterButton: UIButton = UIButton.init(type: (UIButton.ButtonType.custom))
        //set image for button
        filterButton.setImage(UIImage(systemName: "circle.grid.hex"), for: UIControl.State.normal)
        filterButton.setTitle("", for: .normal)
        //add function for button
        filterButton.addTarget(self, action: #selector(showFiltersTable), for: UIControl.Event.touchUpInside)
        //set frame
        filterButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        //backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -15.0, bottom: 0, right: 0);
        
        let rightButton = UIBarButtonItem(customView: filterButton)
        //assign button to navigationbar
        navigationItem.rightBarButtonItem = rightButton
    }
    
    func configureResetFiltersButton() {
        let filterButton: UIButton = UIButton.init(type: (UIButton.ButtonType.custom))
        filterButton.setTitle("RESET".localized, for: UIControl.State.normal)
        filterButton.setTitleColor(.systemBlue, for: .normal)
        //add function for button
        filterButton.addTarget(self, action: #selector(resetPaginationOrFilter), for: UIControl.Event.touchUpInside)
        //set frame
        filterButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        //backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -15.0, bottom: 0, right: 0);
        
        let rightButton = UIBarButtonItem(customView: filterButton)
        //assign button to navigationbar
        navigationItem.rightBarButtonItem = rightButton
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
