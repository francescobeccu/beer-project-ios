//
//  SplashScreen.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//


import Foundation
import UIKit

class SplashScreenViewController: BaseViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var tryAgainButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        indicator.startAnimating()
        
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            // get beers in home page
             self.getBeers()
        }
    }
    
    func getBeers() {
        RestServices.getBeersPaginated(1) { (result, error, objectIds) in
            
            if result == SUCCESS {
                self.goToHome(objectIds)
            } else {
                Utility.showInfoAlert(message: error, fromView: self)
                self.tryAgainButton.isHidden = false
                self.indicator.isHidden = true
            }
        }
    }
    
    @IBAction func tryAgain() {
        self.tryAgainButton.isHidden = true
        self.indicator.isHidden = false
        self.getBeers()
    }
    
    func goToHome(_ objectIds : [NSNumber]) {
        
        let beers = BPCoreDataUtility.shared.getBeerWithId(objectIds)
        Navigation.shared.showHome(self.storyboard!, beers)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

