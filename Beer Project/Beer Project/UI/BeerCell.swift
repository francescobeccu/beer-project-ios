//
//  BeerCell.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import Foundation
import UIKit
import SDWebImage

class BeerCell: UITableViewCell {
    
    @IBOutlet weak var beerImgView: UIImageView!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var beerDescription: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func configureCell(_ beer : Beer) {
        
        self.title.text = beer.beer_name
        self.beerDescription.text = beer.beer_description
        if let image = beer.image_url {
            if let url: URL = URL(string: image) {
                self.beerImgView.sd_setImage(with: url, completed: nil)
            }

        }
    }
    
}
