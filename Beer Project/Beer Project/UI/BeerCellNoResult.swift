//
//  BeerCellNoResult.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import Foundation
import UIKit

class BeerCellNoResult: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
}
