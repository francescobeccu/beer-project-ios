//
//  CoreDataUtility.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import UIKit
import CoreData

class CoreDataUtility: NSObject {
    
    static let shared = CoreDataUtility()
    
    // MARK: get objects from custom predicate
    func getObjects(entityName : String, customPredicate : NSPredicate, orderBy : String) -> [AnyObject]
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        let predicate = customPredicate
        
        let sortDescriptor = NSSortDescriptor(key: orderBy, ascending: false)
        let sortDescriptors = [sortDescriptor]
        request.sortDescriptors = sortDescriptors
        
        request.predicate = predicate
        do {
            let results = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
            return results as [AnyObject]
        }  catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return [];
        }
    }
    
    // MARK: get objects from custom predicate
    func getObjects(entityName : String, customPredicate : NSPredicate) -> [AnyObject]
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        let predicate = customPredicate
        
        request.predicate = predicate
        do {
            let results = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
            return results as [AnyObject]
        }  catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return [];
        }
    }
    
    func getObjectsFromSearch(entityName : String, text : String, key : String, section : Int32) -> [AnyObject]
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        let p1 = NSPredicate(format: "subcategory.category.section.sectionId == \(section)")
        let p2 = NSPredicate(format: "\(key) contains[c] '\(text)'")
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p1, p2])
        
        do {
            let results = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
            return results as [AnyObject]
        }  catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return [];
        }
    }
    
    // MARK: Get All Entities for match
    func getObjects(entityName : String, value : NSObject, key : String) -> AnyObject? {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: CoreDataManager.sharedInstance.persistentContainer.viewContext)
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.predicate = NSPredicate(format: "\(key) == \(value)")
        
        do {
            let result : Array = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(fetchRequest)
            return result as AnyObject
        } catch {
            let fetchError = error as NSError
            print("get objects error \(fetchError)")
            return nil
        }
    }
    
    // MARK: Get objects from ids
    func getObjects(entityName : String, ids : [NSNumber], key : String) -> [AnyObject]
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.predicate = NSPredicate(format: "\(key) IN %@", ids)
        do {
            let results = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
            return results as [AnyObject]
        }  catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return [];
        }
    }
    
    // MARK: Get objects from ids in releationship
    func getObjectsEntityRelation(entityName : String, ids : [NSNumber], entityRelation : String) -> [AnyObject]
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        let predicate = NSPredicate(format: "ANY \(entityRelation) IN %@", ids)
        
        request.predicate = predicate
        do {
            let results = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
            return results as [AnyObject]
        }  catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return [];
        }
    }
    
    // MARK: get objects from custom predicate
    func getObjectsEntityRelation(entityName : String, customPredicate : String) -> [AnyObject]
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        let predicate = NSPredicate(format: "\(customPredicate)")
        
        request.predicate = predicate
        do {
            let results = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
            return results as [AnyObject]
        }  catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return [];
        }
    }
    
    // MARK: Get objects filtered
    func getObjectsEntityFiltered(entityName : String, value : String, keys : [String]) -> [AnyObject]
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        let predicateString : NSMutableString = ""
        
        var i = 0
        for key in keys {
            predicateString.append("\(key) contains [cd] '\(value)'")
            i += 1
            if i < keys.count {
                predicateString.append(" AND ")
            }
        }
        
        let predicate = NSPredicate(format: predicateString as String)
        
        request.predicate = predicate
        do {
            let results = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
            return results as [AnyObject]
        }  catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return [];
        }
    }
    
    // MARK: Get all objects
    func getAllObjects(entityName : String) -> [AnyObject]
    {
        return getAllObjects(entityName : entityName, sort: nil)
    }
    
    func getAllObjects(entityName : String, orderBy : String) -> [AnyObject]
    {
        let sort = NSSortDescriptor(key: orderBy, ascending: true)
        return getAllObjects(entityName : entityName, sort: [sort])
    }
    
    func getAllObjects(entityName : String, sort : [NSSortDescriptor]?) -> [AnyObject]
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        if let _ = sort
        {
            request.sortDescriptors = sort
        }
        do {
            let results = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
            return results as [AnyObject]
        }  catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return [];
        }
    }
    
    func getObjectsEntityFilteredAndSorted(entityName : String, value : String, keys : [String], sort: [NSSortDescriptor]?) -> [AnyObject]
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        if let _ = sort
        {
            request.sortDescriptors = sort
        }
        let predicateString : NSMutableString = ""
        
        var i = 0
        for key in keys {
            predicateString.append("\(key) = [cd] '\(value)'")
            i += 1
            if i < keys.count {
                predicateString.append(" AND ")
            }
        }
        
        let predicate = NSPredicate(format: predicateString as String)
        
        request.predicate = predicate
        do {
            let results = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
            return results as [AnyObject]
        }  catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            return [];
        }
    }
    
    // MARK: count Objects
    func countObjects(entityName : String) -> Int
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        do {
            return try CoreDataManager.sharedInstance.persistentContainer.viewContext.count(for: request)
        }catch {
            return 0
        }
    }
    
    // MARK: count Objects
    func countObjects(entityName : String, value : NSObject, key : String) -> Int
    {        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: CoreDataManager.sharedInstance.persistentContainer.viewContext)
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.predicate = NSPredicate(format: "\(key) == \(value)")
        do {
            return try! CoreDataManager.sharedInstance.persistentContainer.viewContext.count(for: fetchRequest)
        }
    }
    
    // MARK: Delete object
    func deleteObject(object : NSManagedObject) {
        CoreDataManager.sharedInstance.persistentContainer.viewContext.delete(object)
        CoreDataManager.sharedInstance.saveContext()
    }
    
    func deleteObjects(entityName : String, ids : [NSNumber], key : String) {
        let objectsToDetele = getObjects(entityName : entityName, ids: ids, key: key)
        for objectToDelete in objectsToDetele {
            deleteObject(object: objectToDelete as! NSManagedObject)
            //delete(objectToDelete)
        }
    }
    
    // MARK: Delete all objects for Entity
    func deleteAllObjects(entityName : String) {
            let result = getAllObjects(entityName : entityName)
            for singleResult in result {
                deleteObject(object: singleResult as! NSManagedObject)
                //print("Object deleted \(result)")
            }
    }
    
    func deleteAllObjectsForAllEntities() {
        if let entitesByName = CoreDataManager.sharedInstance.persistentContainer.managedObjectModel.entitiesByName as? [String: NSEntityDescription] {
            for (name, _) in entitesByName {
                deleteAllObjects(entityName: name)
            }
        }
    }
    
    // MARK: - fetches
    func executeFetchRequest(request:NSFetchRequest<NSFetchRequestResult>)-> Array<AnyObject>?{
        var results:Array<AnyObject>?
        do {
            try results = CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
        } catch let error {
            print("Error executeFetchRequest: \(error)")
        }
        return results
    }
    
    func executeFetchRequest(request:NSFetchRequest<NSFetchRequestResult>, completionHandler:(_ results: Array<AnyObject>?) -> Void)-> (){
        var results:Array<AnyObject>?
        
        do {
            try results = CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(request)
        } catch let error {
            print("Error executeFetchRequest: \(error)")
        }
        
        completionHandler(results)
    }
    
    // MARK: Get Unique Entity for name
    func uniqueEntityForName(entityName : String) -> AnyObject? {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: CoreDataManager.sharedInstance.persistentContainer.viewContext)
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        do {
            let result : Array = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(fetchRequest)
            if result.count > 0 {
                
                return result.last! as AnyObject
            } else {
                return nil
            }
        } catch {
            let fetchError = error as NSError
            print("Object delete error \(fetchError)")
            return nil
        }
    }
    
    // MARK: Get Entity for name
    func uniqueEntityForName(entityName : String, stringValue : String, key : String) -> AnyObject? {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: CoreDataManager.sharedInstance.persistentContainer.viewContext)
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = NSPredicate(format: "\(key) = %@", stringValue)
        
        do {
            let result : Array = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(fetchRequest)
            if result.count > 0 {
                
                return result.last! as AnyObject
            } else {
                return nil
            }
        } catch {
            let fetchError = error as NSError
            print("Object delete error \(fetchError)")
            return nil
        }
    }
    
    // MARK: Get Entity for name
    func uniqueEntityForName(entityName : String, value : NSObject, key : String) -> AnyObject? {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: CoreDataManager.sharedInstance.persistentContainer.viewContext)
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.predicate = NSPredicate(format: "\(key) == \(value)")
        
        do {
            let result : Array = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(fetchRequest)
            if result.count > 0 {
                
                return result.last! as AnyObject
            } else {
                return nil
            }
        } catch {
            let fetchError = error as NSError
            print("Object delete error \(fetchError)")
            return nil
        }
    }
    
     // MARK: - Remove if not in response
    func removeIfNotInResponse(reponseArray : Array<AnyObject>, keyResponse : String, entityName : String, entityKey : String) {
        let all = self.getAllObjects(entityName: entityName)
        
        for entity : AnyObject in all {
            var found = false
            
            for dictionary : NSDictionary in reponseArray as! [NSDictionary] {
                let paramsValue = dictionary.value(forKey: keyResponse) as! Int
                let dbValue = entity.value(forKey: entityKey) as! Int
                
                if dbValue == paramsValue {
                    found = true
                }
            }
            if !found {
                print(entity)
                self.deleteObject(object: entity as! NSManagedObject)
            }
        }
        
        CoreDataManager.sharedInstance.saveContext()
    }
    
    func saveIfNotSingletonExist(entityName : String) -> NSManagedObject!{
        var managedObject : NSManagedObject? = CoreDataUtility.shared.uniqueEntityForName(entityName: entityName) as? NSManagedObject
        if managedObject == nil
        {
            //not exist
            managedObject = NSEntityDescription.insertNewObject(forEntityName: entityName, into: CoreDataManager.sharedInstance.persistentContainer.viewContext)
        }
        return managedObject
    }
    
    func saveIfNotExist(entityName : String, value : NSObject, key : String) -> NSManagedObject!{
        var managedObject : NSManagedObject? = CoreDataUtility.shared.uniqueEntityForName(entityName : entityName, value: value, key: key) as? NSManagedObject
        if managedObject == nil
        {
            //not exist
            managedObject = NSEntityDescription.insertNewObject(forEntityName: entityName, into: CoreDataManager.sharedInstance.persistentContainer.viewContext)
        }
        return managedObject
    }
    
    func createNewObject (entityName : String) -> NSManagedObject!
    {
        let managedObject = NSEntityDescription.insertNewObject(forEntityName: entityName, into: CoreDataManager.sharedInstance.persistentContainer.viewContext)
        return managedObject
    }
    
    // MARK: - Application's Documents directory
    // Returns the URL to the application's Documents directory.
    var applicationDocumentsDirectory: NSURL {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.endIndex-1] as NSURL
    }
    
    // MARK: - DB Options
    func databaseOptions() -> Dictionary <String,Bool> {
        var options =  Dictionary<String,Bool>()
        options[NSMigratePersistentStoresAutomaticallyOption] = true
        options[NSInferMappingModelAutomaticallyOption] = true
        return options
    }
    
    
    func fetchRecordsForEntity(_ entity: String) -> [NSManagedObject] {
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        
        // Helpers
        var result = [NSManagedObject]()
        
        do {
            // Execute Fetch Request
            let records = try CoreDataManager.sharedInstance.persistentContainer.viewContext.fetch(fetchRequest)
            
            if let records = records as? [NSManagedObject] {
                result = records
            }
            
        } catch {
            print("Unable to fetch managed objects for entity \(entity).")
        }
        
        return result
    }

    
   
}
