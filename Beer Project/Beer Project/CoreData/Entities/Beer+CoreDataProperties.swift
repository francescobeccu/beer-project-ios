//
//  Beer+CoreDataProperties.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//
//

import Foundation
import CoreData


extension Beer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Beer> {
        return NSFetchRequest<Beer>(entityName: "Beer")
    }

    @NSManaged public var beer_id: Int32
    @NSManaged public var beer_name: String?
    @NSManaged public var tagline: String?
    @NSManaged public var first_brewed: String?
    @NSManaged public var beer_description: String?
    @NSManaged public var image_url: String?
    @NSManaged public var contributed_by: String?
    @NSManaged public var brewers_tips: String?
    @NSManaged public var food_pairing: NSObject?
    @NSManaged public var ingredients: NSObject?
    @NSManaged public var method: NSObject?
    @NSManaged public var boil_volume: NSObject?
    @NSManaged public var volume: NSObject?
    @NSManaged public var attenuation_level: Float
    @NSManaged public var ph: Float
    @NSManaged public var srm: Float
    @NSManaged public var ebc: Float
    @NSManaged public var target_og: Float
    @NSManaged public var target_fg: Float
    @NSManaged public var ibu: Float
    @NSManaged public var abv: Float

}

extension Beer : Identifiable {

}
