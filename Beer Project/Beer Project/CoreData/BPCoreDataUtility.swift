//
//  BPCoreDataUtility.swift
//  Beer Project
//
//  Created by Francesco Beccu on 28/09/21.
//

import Foundation
class BPCoreDataUtility: NSObject {
    
    static let shared = BPCoreDataUtility()
    
    // MARK: Save Beer
    func saveBeers(_ beersDTO : NSArray) -> [NSNumber] {
        var beersId : [NSNumber] = []
        for beerDTO in beersDTO {
            beersId.append(self.saveBeer(beerDTO as! NSDictionary))
        }
        return beersId
    }
    
    func saveBeer(_ beerDTO : NSDictionary) -> NSNumber {
        let beer_id = beerDTO.value(forKey: "id") as? Int32
        
        let entity : Beer = CoreDataUtility.shared.saveIfNotExist(entityName: "Beer", value: beer_id! as NSObject, key: "beer_id") as! Beer
        
        let beer_name = beerDTO.value(forKey: "name") as? String
        let tagline = beerDTO.value(forKey: "tagline") as? String
        let first_brewed = beerDTO.value(forKey: "first_brewed") as? String
        let beer_description = beerDTO.value(forKey: "description") as? String
        let image_url = beerDTO.value(forKey: "image_url") as? String
        let contributed_by = beerDTO.value(forKey: "contributed_by") as? String
        let brewers_tips = beerDTO.value(forKey: "brewers_tips") as? String
        let food_pairing = beerDTO.value(forKey: "food_pairing") as? NSArray
        let ingredients = beerDTO.value(forKey: "ingredients") as? NSDictionary
        let method = beerDTO.value(forKey: "method") as? NSDictionary
        let boil_volume = beerDTO.value(forKey: "boil_volume") as? NSDictionary
        let volume = beerDTO.value(forKey: "volume") as? NSDictionary
        let attenuation_level = beerDTO.value(forKey: "attenuation_level") as? Float
        let ph = beerDTO.value(forKey: "ph") as? Float
        let srm = beerDTO.value(forKey: "srm") as? Float
        let ebc = beerDTO.value(forKey: "ebc") as? Float
        let target_og = beerDTO.value(forKey: "target_og") as? Float
        let target_fg = beerDTO.value(forKey: "target_fg") as? Float
        let ibu = beerDTO.value(forKey: "ibu") as? Float
        let abv = beerDTO.value(forKey: "abv") as? Float
        
        entity.beer_id = beer_id!
        entity.beer_name = beer_name
        entity.tagline = tagline
        entity.first_brewed = first_brewed
        entity.beer_description = beer_description
        entity.image_url = image_url
        entity.contributed_by = contributed_by
        entity.brewers_tips = brewers_tips
        entity.food_pairing = food_pairing
        entity.ingredients = ingredients
        entity.method = method
        entity.boil_volume = boil_volume
        entity.volume = volume
        entity.attenuation_level = attenuation_level ?? 0.0
        entity.ph = ph ?? 0.0
        entity.srm = srm ?? 0.0
        entity.ebc = ebc ?? 0.0
        entity.target_og = target_og ?? 0.0
        entity.target_fg = target_fg ?? 0.0
        entity.ibu = ibu ?? 0.0
        entity.abv = abv ?? 0.0
        
        CoreDataManager.sharedInstance.saveContext()
        
        return NSNumber(value: beer_id!)
    }
    
    func getAllBeer() -> [Beer] {
        let beers = CoreDataUtility.shared.getAllObjects(entityName: "Beer") as! [Beer]
        return beers
    }
    
    
    func getBeerWithId(_ beerId : [NSNumber]) -> [Beer] {
        let beers = CoreDataUtility.shared.getObjects(entityName: "Beer", ids: beerId, key: "beer_id") as! [Beer]
        return beers
    }
}
